<?php
$discord_webhook = 'DISCORD_WEBHOOK';
$telegram = ['BOT_TOKEN','CHAT_ID']; 

$urls = explode("\n",file_get_contents('urls.txt'));
$tmpName = '.tmp_'.rand();
file_put_contents($tmpName,'');

// single
if(isset($argv[1])){
	$id=$argv[1];
	$url = $urls[$argv[1]];
	if(!strstr($url,"##")&&$url!=''){
		get($url,$id);
	}else{
		unlink($tmpName);
		die;
	}
}else{
	// mass
	$id=1;
	echo "\e[33;1m --> TMP FILE: $tmpName\n\n";
	foreach($urls as $url){
		if(isset($url)&&!strstr($url,"##")&&$url!=''){
			get($url,$id);
			$id++;
		}
	}
}

// ---------- Add to db and send to discord ------------
$db = file_get_contents('db.txt');
$posts = explode("\n",file_get_contents($tmpName));
foreach($posts as $post){
	if($post != ''){
		$post = explode("==>>",$post);
		$title = $post[0];
		$link = $post[1];
		$date = $post[2];

		$data = $title.'==>'.$link.'==>'.$date;

		if (!strstr($db,$data)){
			$fh = fopen('db.txt','a');
			fwrite($fh , $data."\n");
			fclose($fh);
			echo "\e[32;1m New \e[37;1m--> \e[33;1m$title | \e[34;1m$link \e[36;1m # $date\n";
			send($title,$link,$date);
		}
	}
}
unlink($tmpName);


function get($url,$id){
	global $tmpName;
	$all = '';
	$request = request($url);
	if($request[1] != 200)
		$feed = simplexml_load_file($url);
	else
		$feed = simplexml_load_string($request[0]);

	echo "\e[37;1m[$id] Host: ".parse_url($url,PHP_URL_HOST)."\n";
	if(isset($feed->channel->item)){
		foreach($feed->channel->item as $entry) {
			$title = $entry->title;
			$link = set_http($entry->link);
			$date = $entry->pubDate;
			if($date == '')
				$date = $entry->updated;

			// echo "\e[35;1m$title \e[33;1m($link) \e[34;1m| \e[36;1m$date \n";
			$all .= $title.'==>>'.$link.'==>>'.$date."\n";
		}
	}else{
		foreach($feed->entry as $entry) {
			$title = $entry->title;
			$link = set_http($entry->id);
			$date = $entry->published;
			if($date == '')
				$date = $entry->updated;


			// echo "\e[35;1m$title \e[33;1m($link) \e[34;1m| \e[36;1m$date \n";
			$all .= $title.'==>>'.$link.'==>>'.$date."\n";
		}
	}

	// write file 
	$fh = fopen($tmpName, 'a');
	fwrite($fh , $all);
	fclose($fh);

	// echo "\n\n\n";
}

function request($url){
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    $output = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch); 
    return [$output,$status];
}

function set_http($link){
	$arr = str_split($link);
	if( $arr[0]!='h' || $arr[1]!='t' || $arr[2]!='t' || $arr[3]!='p' ){
		return 'https://'.$link;
	}else{
		return $link;
	}
}
function send($title,$link,$date){
	global $discord_webhook , $telegram ;

	// ================ send to telegram ================ 
    $text = "$title\n--> $link\n# $date";
    $data = ['chat_id' => urlencode($telegram[1]),'text' => $text];
    $url = "https://api.telegram.org/bot$telegram[0]/sendMessage";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($data));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);


	// send to discord
	$msg_data=json_encode(["username"=>"Writeups","content"=>"**$title**\n====> $link\n($date)"],JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	$ch = curl_init($discord_webhook);
	curl_setopt($ch,CURLOPT_HTTPHEADER,['Content-type: application/json']);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$msg_data);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$run = curl_exec($ch);
	curl_close($ch);
}
/// -----> by @mr.msa3